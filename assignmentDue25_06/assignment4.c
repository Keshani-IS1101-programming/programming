#include <stdio.h>

int revenue1(int,int);
int cost1(int);
int profit1(int, int);
int findquantity1(int);

int main(){
	int p=24; //user can change prices and check at which price he can earn maximum profits
	
	int q=findquantity1(p);
	int rev=revenue1(p,q);
	int cost=cost1(q);
	int profit=profit1(rev,cost);
	
	printf("price= %d\n",p);
	printf("revenue= %d\n",rev);
	printf("cost= %d\n",cost);
	printf("profit= %d\n",profit);

}

int findquantity1(int p1){
	int q=120-(20/5)*(p1-15);
	return q;
}
	
//find revenue function
int revenue1(int p1, int q1){
	int r=p1*q1;
	return r;
}
//find cost function

int cost1(int q2){
	int c=500+q2*3;
	return c;
}

//find profit function

int profit1(int r1,int c1){
	int p=r1-c1;
	return p;
}