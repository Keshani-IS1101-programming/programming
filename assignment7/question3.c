#include <stdio.h>
#include <string.h>

#define ROWS 2
#define COLS 2

void read(int [ROWS][COLS]);
int print(int [ROWS][COLS]);
int sumMatrix(int [ROWS][COLS], int [ROWS][COLS], int [ROWS][COLS]);
int multiplication(int[ROWS][COLS], int [ROWS][COLS], int [ROWS][COLS]);

int main(){
int m1[ROWS][COLS];
int m2[ROWS][COLS];
int sum[ROWS][COLS];
int multi[ROWS][COLS]={0};

read(m1);
print(m1);

read(m2);
print(m2);
sumMatrix(m1,m2,sum);
multiplication(m1,m2,multi);
printf("\n");
return 0;
}


//get values to the multi dimensional array- Matrix
void read(int m[ROWS][COLS]){
	printf("enter a matrix in the format %d x %d: ",ROWS,COLS);
	for(int i=0; i<ROWS;i++)
		for(int j=0;j<COLS;j++)
			scanf("%d",&m[i][j]);
}

//print user input values for the array  
int print(int m[ROWS][COLS]){
	for(int i=0; i<ROWS;i++)
		for(int j=0;j<COLS;j++)
			printf("%d ",m[i][j]);
		printf("\n");
} 

//calculate the sum of 2 matrixes
int sumMatrix(int m1[ROWS][COLS], int m2[ROWS][COLS],	int sum[ROWS][COLS])
{
	printf("sum of given matrices: ");
	for(int i=0; i<ROWS;i++)
	{
		for(int j=0;j<COLS;j++)
		{
		 sum[i][j]=m1[i][j]+m2[i][j];
		 printf("%d ",sum[i][j]);
		}
	}
}

//multiply given matrixes
int multiplication(int m1[ROWS][COLS], int m2[ROWS][COLS],	int multi[ROWS][COLS]){
	printf("\nresult after mutiplying given matrices: ");
	for(int i=0;i<ROWS;i++){
		for(int j=0; j<COLS;j++){
			for(int k=0;k<2;k++){
				multi[i][j]+=(m1[i][k])*(m2[k][j]);
			}
			printf("%d ",multi[i][j]);
		}
	}
	
}
