#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void reverse(char sen[], int i,int j,int start, int end)
{
	for(i=end; i>=0; i--)
	{
		if(sen[i]==' ' || i==0)
		{
			if(i==0)
				start=0;
			else
				start=i+1;
								
			for(j=start; j<=end; j++)
			{
				printf("%c", sen[j]);
			}
			end=i-1;
			printf(" ");				
		} 
	}
}

int main(){
	char sen[100];
	int i, j, start;

	printf("enter sentence: ");
	fgets(sen,sizeof(sen),stdin);

	int end=strlen(sen)-1;
	
	printf("sentence in reverse order: ");

	reverse(sen,i,j,start,end);
	printf("\n");
	return 0;
}