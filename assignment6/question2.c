#include <stdio.h>

int fibonacci(int n){
if (n==0) return 0;
else if (n == 1) return 1;
else return fibonacci(n-1)+fibonacci(n-2);
}

int main(){
	int i, f, x;
	printf("enter the number to get fibonacci sequence: ");
	scanf("%d",&x);
	for (i=0;i<=x;i++)
	{
	f=fibonacci(i);
	printf("%d\n",f);
	}
	return 0;
}
