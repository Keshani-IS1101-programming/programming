#include <stdio.h>
//prints a row
int pattern(int n){
	printf("%d",n);
	if(n>1)
		pattern(n-1);
	else 
		printf("\n");
}
// print given number of rows
int patternFinal(int rowNum, int noOfRows){
	if (rowNum<=noOfRows)
	{
		pattern(rowNum);
		patternFinal(rowNum+1, noOfRows);
	}
}


int main(){
	//no of rows
	int r;
	printf("enter no of rows needed: ");
	scanf("%d",&r);
	printf("\n");
	patternFinal(1,r);

	
	
	return 0;

}